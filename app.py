from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/', methods=['GET'])
def Index():
    name = request.args.get('name')
    message = request.args.get('message')
    return render_template('index.html', name=name, message=message)

if __name__ == '__main__':
    print('SYS> Attempting to start web app...')
    app.run(debug=False)
